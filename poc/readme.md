# KWidgetsAddons demo

This is a small proof of concept of the Python bindings for KDE Frameworks. The 
KSeparator widget is used in a PySide6 application. It has been tested on Arch Linux 
with the following versions:

- Qt 6.6.2
- KDE Frameworks 6.0.0
- PySide6 6.6.2
- shiboken6 6.6.2

## Building

```
$ shiboken6 --enable-pyside-extensions -T/usr/share/PySide6/typesystems/ -I/usr/include/KF6/KWidgetsAddons/ -I/usr/include/KF6/ -I/usr/include/KF6/KWidgetsAddons/ -I/usr/include/qt6/ -I/usr/include/qt6/QtWidgets/ kwidgetsaddons_global.h typesystem_kwidgetsaddons.xml
$ python setup.py build
```

Now you can run the test application with something similar to:

```
$ PYTHONPATH=build/lib.linux-x86_64-cpython-311 python test.py
```
