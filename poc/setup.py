from distutils.core import setup, Extension

module1 = Extension('KWidgetsAddons',
                    include_dirs = ['/usr/include/qt6/', '/usr/include/KF6/', '/usr/include/KF6/KWidgetsAddons/', '/usr/include/qt6/QtWidgets/', '/usr/include/shiboken6/', '/usr/include/PySide6/',
                    '/usr/include/PySide6/QtWidgets/', '/usr/include/PySide6/QtGui/', '/usr/include/PySide6/QtCore/', '/usr/include/qt6/QtCore/', '/usr/include/qt6/QtWidgets/', '/usr/include/qt6/QtGui/'],
                    libraries = ['KF6WidgetsAddons', 'Qt6Widgets', 'Qt6Core', 'Qt6Gui', 'shiboken6.abi3', 'pyside6.abi3'],
                    library_dirs = ['/usr/lib'],
                    sources = ['out/KWidgetsAddons/kwidgetsaddons_module_wrapper.cpp', 'out/KWidgetsAddons/kseparator_wrapper.cpp'])

setup (name = 'KWidgetsAddons',
       version = '1.0',
       description = 'This is a demo package',
       author = 'Name',
       author_email = 'example@example.com',
       url = 'https://docs.python.org/extending/building',
       long_description = '''
This is really just a demo package.
''',
       ext_modules = [module1])

