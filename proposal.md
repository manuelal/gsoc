# Python bindings for KDE Frameworks

## Introduction

KDE Frameworks is a collection of C++ addon libraries to Qt that provide common 
functionality for desktop applications built with the Qt framework. While Qt itself 
provides support for developing applications using other programming languages, such 
as Python, KDE Frameworks does not.

Other desktop enviroments, such as GNOME, provide a broad range of bindings for their 
libraries. The variety in the available programming languages to choose has allowed 
for the development of many new applications, specially in recent years.

This project aims to close the gap with other platform technologies adding Python 
bindings to the most common frameworks.

## Project goals

- Creating a Python library which can be used along PySide.
- Upload the library to the Python Package Index (pypi.org).
- Adding developer documentation on how to use the library.
- Automatizing the building & testing process with CI on invent.kde.org.
- Being easy to maintain over the years and to add support for more frameworks.

## Implementation

We will use [Shiboken], a library to generate Python bindings from C++ code. It is 
used as the base of the PySide project (Qt for Python). The bindings are generated 
automatically from a XML definition and some small amount of C++ code. A working 
example of the XML definition for the KSeparator widget is the following (see the 
[full demo][demo]):

```xml
<?xml version="1.0"?>
<typesystem package="KWidgetsAddons">
    <load-typesystem name="typesystem_widgets.xml" generate="no"/>

    <object-type name="KSeparator" />

</typesystem>
```

Each library is represented by a `typesystem` node that contains all the classes, 
enumerations and functions of the package. Classes (like `KSeparator`) are defined 
using the `object-type` node.

Apart from the XML file, Shiboken also needs a C++ header file that includes all the 
necesaries headers of the classes used in the XML file. C++ code can also be used to 
customize the generated code in case it needs some customizations.

We aim to add support to the following frameworks (in no particular order):

- KWidgetsAddons
- KCoreAddons
- KI18n

As these are libraries with a high amount of classes we may not be able to add all of 
them during the project. It is expected that the pace of development increases once 
we familiarize with the process of adding new ones. However, support for more 
frameworks may be added if time allows.

As part of the project, we will also write the corresponding developer documentation 
on how to install and use the library.

## Timeline

We will follow the standard 12 weeks timeline for a 350 h project, adding two weeks 
for vacation and unplanned circunstances that may occur during the program.

| Week | Summary |
| ---- | ------- |
| Week 1 | Investigation, creating blank Python library, documentation site |
| Week 2 | Starting to port KWidgetsAddons |
| Week 3 | Porting KWidgetsAddons |
| Week 4 | Porting KWidgetsAddons |
| Week 5 | Porting KWidgetsAddons |
| Week 6 | Adding CI support to the library (can be done early or later) |
| Week 7 | Porting KCoreAddons |
| Week 8 | Porting KCoreAddons |
| Week 9 | Porting KCoreAddons |
| Week 10 | Porting KCoreAddons |
| Week 11 | Porting KI18n |
| Week 12 | Porting KI18n |

## About me

I am Manuel Alcaraz Zambrano, a Software Engineer student from Spain. I have won the 
2019 Google Code-in contest with Wikimedia. I have previously made some small 
contributions to KDE projects, such as [fixing a dark theme issue in KTouch][patch].

You can contact me using Matrix (@alcarazzam:kde.org).


[Shiboken]: https://doc.qt.io/qtforpython-6/shiboken2/index.html
[patch]: https://phabricator.kde.org/D23048
[demo]: https://invent.kde.org/manuelal/gsoc/-/tree/master/poc?ref_type=heads
